#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass book
\begin_preamble
\frenchspacing
\usepackage{hyperref}
\usepackage{amsmath}
\newcommand{\ø}{\raisebox{.3ex}{\tiny$\; \bullet \;$}}
\renewcommand{\arraystretch}{1.25}
\usepackage{mathpazo}
\usepackage{color}
\usepackage{graphicx}
\usepackage{multicol}

\RequirePackage[dvipsnames]{xcolor} % [dvipsnames]
\definecolor{halfgray}{gray}{0.55} % chapter numbers will be semi transparent .5 .55 .6 .0
\definecolor{webgreen}{rgb}{0,.5,0}
\definecolor{webbrown}{rgb}{.6,0,0}
\definecolor{Maroon}{cmyk}{0, 0.87, 0.68, 0.32}
\definecolor{RoyalBlue}{cmyk}{1, 0.50, 0, 0}
\definecolor{Black}{cmyk}{0, 0, 0, 0}
\newcommand{\stitle}[1]{{\textbf{\color{RoyalBlue} #1}}}
\newcommand{\mail} [1]{{\href{mailto:#1}{#1}}}

\hypersetup{
    colorlinks=true, linktocpage=true, pdfstartpage=3, pdfstartview=FitV,%
    breaklinks=true, pdfpagemode=UseNone, pageanchor=true, pdfpagemode=UseOutlines,
    plainpages=false, bookmarksnumbered, bookmarksopen=true, bookmarksopenlevel=1,
    hypertexnames=true, pdfhighlight=/O,
    urlcolor=RoyalBlue, linkcolor=RoyalBlue, 
    citecolor=webgreen, 
    pdfauthor = {NOME COGNOME},
    pdfcreator  = {LaTeX with hyperref package},
    pdfproducer = {pdfeTeX},
    plainpages=false,
    pdfpagelabels
}
\end_preamble
\use_default_options false
\maintain_unincluded_children false
\language italian
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize 10
\spacing single
\use_hyperref false
\papersize a4paper
\use_geometry false
\use_package amsmath 1
\use_package amssymb 2
\use_package cancel 1
\use_package esint 0
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 0
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 0
\index Indice
\shortcut idx
\color #008000
\end_index
\leftmargin 3.5cm
\topmargin 3.5cm
\rightmargin 3.5cm
\bottommargin 3.5cm
\headsep 1cm
\secnumdepth 3
\tocdepth 3
\paragraph_separation skip
\defskip medskip
\quotes_language swedish
\papercolumns 1
\papersides 2
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Section
RSA
\end_layout

\begin_layout Standard
RSA è stato sviluppato nel 1977 e pubblicato per la prima volta nel 1978.
 Da allora è il più usato tra gli algoritmi di cifratura a chiave pubblica.
 RSA è un cifrario a blocchi nel quale il plaintext e il ciphertext sono
 considerati numeri interi compresi nell'intervallo 
\emph on
[0, n-1]
\emph default
, per qualche 
\emph on
n
\emph default
.
 La dimensione tipica, in bit, di 
\emph on
n 
\emph default
è 1024 bit.
 Il plaintext è cifrato a blocchi la cui dimensione è 
\emph on
i
\emph default
 tale che 
\begin_inset Formula $2^{i}<n\leq2^{i+1}$
\end_inset

.
 Detto M il plaintext e C il ciphertext, cifratura e decifratura assumono
 la seguente forma:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
C=M^{e}mod\,n
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
M=C^{d}mod\,n=(M^{e})^{d}mod\,n=M^{ed}mod\,n
\]

\end_inset


\end_layout

\begin_layout Standard
Sia chi cifra il messaggio che chi dovrà decifrarlo deve conoscere il valore
 di 
\emph on
n
\emph default
.
 Chi cifra il messaggio deve conoscere il valore di 
\emph on
e
\emph default
, mentre l'unico a conoscere il valore di 
\emph on
d
\emph default
 è chi dovrà decifrare il messaggio.
 La chiave pubblica, dunque, è costituita dalle la coppia 
\emph on
PU={e,n}
\emph default
 mentre la chiave privata è costituita dalla coppia 
\emph on
PR={d,n}
\emph default
.
 Affinché tale algoritmo soddisfi i requisiti per uno schema di cifratura
 a chiave pubblica, devono essere soddisfatte le seguenti:
\end_layout

\begin_layout Itemize
deve essere possibile trovare i valori di 
\emph on
e
\emph default
, 
\emph on
d
\emph default
, 
\emph on
n
\emph default
 tali che 
\begin_inset Formula $M^{ed}mod\,n=M$
\end_inset

;
\end_layout

\begin_layout Itemize
deve essere relativamente semplice calcolare 
\begin_inset Formula $M^{e}mod\,n$
\end_inset

 ed 
\begin_inset Formula $C^{d}mod\,n$
\end_inset

, per ogni valore di 
\emph on
M<n;
\end_layout

\begin_layout Itemize
deve essere infattibile determinare 
\emph on
d
\emph default
 dati 
\emph on
e
\emph default
 ed 
\emph on
n
\emph default
.
\end_layout

\begin_layout Subsection
Generazione della coppia di chiavi
\end_layout

\begin_layout Standard
Affinché sia soddisfatta la relazione
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
M^{ed}mod\,n=M
\]

\end_inset


\end_layout

\begin_layout Standard
è necessario che 
\emph on
e
\emph default
 e 
\emph on
d
\emph default
 siano inversi moltiplicativi modulo 
\begin_inset Formula $\phi(n)$
\end_inset

, con 
\begin_inset Formula $\phi(n)$
\end_inset

 funzione 
\begin_inset Formula $\phi$
\end_inset

 di Eulero.
\end_layout

\begin_layout Standard
L'algoritmo RSA prevede che ogni partecipante crei una coppia di chiavi,
 una pubblica ed una privata, seguendo i seguenti passi:
\end_layout

\begin_layout Enumerate
ogni partecipante seleziona due numeri primi grandi, 
\emph on
p
\emph default
 e 
\emph on
q
\emph default
, grandi tali 
\begin_inset Formula $p\neq q$
\end_inset

;
\end_layout

\begin_layout Enumerate
ogni partecipante calcola 
\begin_inset Formula $n=p·q$
\end_inset

;
\end_layout

\begin_layout Enumerate
ogni partecipante calcola la funzione 
\begin_inset Formula $\phi$
\end_inset

 di Eulero, 
\begin_inset Formula $\phi(n)=(p-1)(q-1)$
\end_inset

;
\end_layout

\begin_layout Enumerate
ogni partecipante calcola (o sceglie) 
\emph on
e
\emph default
 in modo tale che sia relativamente primo con 
\emph on

\begin_inset Formula $\phi(n)$
\end_inset


\begin_inset Foot
status collapsed

\begin_layout Plain Layout
Due numeri si dicono relativamente primi se il loro Massimo Comun Divisore
 è 1
\end_layout

\end_inset


\emph default
 e che sia minore di esso
\emph on
;
\end_layout

\begin_layout Enumerate
ogni partecipante calcola 
\emph on
d
\emph default
, inverso moltiplicativo di 
\emph on
e
\emph default
 modulo 
\begin_inset Formula $\phi(n)$
\end_inset

, cioè 
\begin_inset Formula $d\,|\,e\text{·}d\,mod\phi(n)=1$
\end_inset

;
\end_layout

\begin_layout Enumerate
ogni partecipante costituisce le coppie 
\emph on
PU={n,e}
\emph default
 e 
\emph on
PR={d,n};
\end_layout

\begin_layout Enumerate
ogni partecipante pubblica la coppia 
\emph on
PU
\emph default
 e mantiene segreta la coppia 
\emph on
PR
\emph default
.
\end_layout

\begin_layout Standard
Cifratura e decifratura avvengono come indicato precedentemente.
\end_layout

\begin_layout Paragraph
Esempio:
\end_layout

\begin_layout Enumerate
selezionati due numeri primi, 
\begin_inset Formula $p=17$
\end_inset

e 
\begin_inset Formula $q=11$
\end_inset

;
\end_layout

\begin_layout Enumerate
calcoliamo 
\begin_inset Formula $n=p\cdot q=187$
\end_inset

 
\end_layout

\begin_layout Enumerate
calcoliamo 
\begin_inset Formula $\phi(n)=16\cdot10=160$
\end_inset


\end_layout

\begin_layout Enumerate
scegliamo 
\begin_inset Formula $e=7$
\end_inset

 (che, essendo primo, sarà sicuramente relativamente primo con 
\begin_inset Formula $\phi(n)$
\end_inset

);
\end_layout

\begin_layout Enumerate
calcoliamo 
\begin_inset Formula $d\,:\,e\cdot d\,mod\,\phi(n)=1$
\end_inset

, usando 
\begin_inset Quotes sld
\end_inset

Extended Euclid
\begin_inset Quotes srd
\end_inset

 (vedi paragrafo 
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:Determinare-l'inverso-in-modulo"

\end_inset

), 
\begin_inset Formula $d=23$
\end_inset

;
\end_layout

\begin_layout Enumerate
costituiamo le coppie PU={187,7} e PR={187,23}, pubblicando la prima e tenendo
 segreta la seconda.
\end_layout

\begin_layout Standard
Supponiamo di voler cifrare, e successivamente decifrare, il numero 88.
 Per quanto detto precedentemente, il ciphertext sarà 
\begin_inset Formula $88^{8}mod\,187=11$
\end_inset

, mentre il plaintext, ottenuto mediante decifratura, sarà 
\begin_inset Formula $11^{23}mod\,187=88$
\end_inset

.
\end_layout

\begin_layout Subsection
Aspetti computazionali
\end_layout

\begin_layout Subsubsection
Numeri primi grandi
\end_layout

\begin_layout Standard
Anziché pre-calcolare un insieme di numeri primi grande, alcune implementazioni
 di RSA usano un approccio diverso: scelto un numero casuale grande, effettuano
 un test di primalità, con l'obbiettivo di stabilire se esso sia un numero
 primo.
 I test di primalità esatti sono molto complessi, per cui, molto spesso,
 ci si accontenta di test probabilistici, cioè test che sono in grado di
 stabilire, con una probabilità tendente ad uno, se un numero sia primo.
\end_layout

\begin_layout Standard
Un primo test fa uso delle proprietà dell'esponenziazione in modulo, che
 saranno meglio discusse al paragrafo 
\begin_inset CommandInset ref
LatexCommand ref
reference "subsec:Esponenziazione-in-modulo"

\end_inset

.
 Diciamo che un numero 
\emph on
n
\emph default
 è 
\emph on
psaudoprimo 
\emph default
in base 
\emph on
a 
\emph default
se 
\begin_inset Formula $a^{n-1}=1(mon\,n)$
\end_inset

.
 Il teorema di Fermat afferma che se 
\emph on
n 
\emph default
è primo soddisfa l'equazione precedente, mentre il viceversa è 
\begin_inset Quotes sld
\end_inset

quasi
\begin_inset Quotes srd
\end_inset

 vero, nel senso che è molto probabile che se tale equazione è rispettata
 il numero è primo con una probabilità tendente ad uno: è possibile dimostrare
 che la probabilità che venga commesso un errore, stabilendo che un numero
 sia primo quando in realtà non lo è, tende a zero al crescere del numero
 di bit con il quale è rappresentato il numero stesso.
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "numbers=left,numberstyle={\scriptsize},basicstyle={\scriptsize\ttfamily},showstringspaces=false"
inline false
status open

\begin_layout Plain Layout

Pseudoprime(n)
\end_layout

\begin_layout Plain Layout

if ModularExponentiation(2, n-1, n) != 1
\end_layout

\begin_layout Plain Layout

	return "Composto"	// Esatto
\end_layout

\begin_layout Plain Layout

else
\end_layout

\begin_layout Plain Layout

	return "Primo"		// Forse!
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Questo test viene usato in tantissime applicazioni pratiche, tra cui alcune
 implementazioni libere di RSA.
 Un test più preciso è il test di Miller-Rabin.
 
\end_layout

\begin_layout Subsubsection
\begin_inset CommandInset label
LatexCommand label
name "subsec:Determinare-l'inverso-in-modulo"

\end_inset

Determinare l'inverso mudulo n
\end_layout

\begin_layout Standard
Senza scendere troppo nel dettaglio, la nozione di divisibilità è alla base
 della teoria dei numeri.
 Presi 
\begin_inset Formula $a,d>0$
\end_inset

, la notazione 
\emph on
d|a
\emph default
 (leggi “d divide a”) significa che 
\begin_inset Formula $\exists k>0\,:\,a=k\cdot d$
\end_inset

.
 Se per un intero 
\emph on

\begin_inset Formula $a>1$
\end_inset


\emph default
 l'unico divisore è 1, a viene detto 
\emph on
numero primo
\emph default
.
 Dato un intero n, si può dividere i numeri interi in quelli multipli di
 n e quelli che, invece, non sono multipli di n.
 Per ogni intero 
\emph on
a
\emph default
 ed ogni intero positivo 
\emph on
n
\emph default
, esiste un unico valore 
\emph on
q
\emph default
 ed un unico valore 
\begin_inset Formula $r\in(0,n)$
\end_inset

 tale che 
\begin_inset Formula $a=qn+r$
\end_inset

.
 Il valore 
\begin_inset Formula $q=\left\lfloor a/n\right\rfloor $
\end_inset

 è detto quoziente, mentre 
\begin_inset Formula $r=a\,mod\,n$
\end_inset

 è detto resto.
 n|a se e solo se r=0.
 Se 
\begin_inset Formula $d$
\end_inset

 è divisore di 
\emph on
a
\emph default
 (cioè d|a) ed è anche divisore di 
\emph on
b
\emph default
, allora 
\emph on
d
\emph default
 è un divisore comune di 
\emph on
a
\emph default
 e 
\emph on
b
\emph default
.
 Il più grande dei divisori comuni di 
\emph on
a
\emph default
 e 
\emph on
b
\emph default
 è detto 
\emph on
massimo comun divisore
\emph default
, MCD, ed è indicato con la notazione gcd(a,b).
 Si può dimostrare che 
\begin_inset Formula $gcd(a,b)=gcd(b,a\,mod\,b)$
\end_inset

.
 L'algoritmo di Euclide, che consente di calcolare il massimo comun divisore
 tra due numeri, è basato proprio su quest'ultima equazione.
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "numbers=left,numberstyle={\scriptsize},basicstyle={\scriptsize\ttfamily},showstringspaces=false,tabsize=4"
inline false
status open

\begin_layout Plain Layout

Euclid(a, b)
\end_layout

\begin_layout Plain Layout

if b == 0
\end_layout

\begin_layout Plain Layout

	return a;
\end_layout

\begin_layout Plain Layout

else
\end_layout

\begin_layout Plain Layout

	return Euclid(b, a mod b);
\end_layout

\end_inset


\end_layout

\begin_layout Standard
L'algoritmo di Euclide esteso, invece che restituire un solo valore, corrisponde
nte al massimo comun divisore tra due numeri 
\emph on
a
\emph default
 e 
\emph on
b
\emph default
, restituisce una terna di valori, 
\emph on
d
\emph default
, 
\emph on
x
\emph default
 ed 
\emph on
y
\emph default
, tali che 
\begin_inset Formula $d=gcd(a,b)=ax+by$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "numbers=left,numberstyle={\scriptsize},basicstyle={\scriptsize\ttfamily},showstringspaces=false,tabsize=4"
inline false
status open

\begin_layout Plain Layout

EetendedEuclid(a, b)
\end_layout

\begin_layout Plain Layout

if b == 0
\end_layout

\begin_layout Plain Layout

	return (a, 1, 0);
\end_layout

\begin_layout Plain Layout

else
\end_layout

\begin_layout Plain Layout

	(d',x', y')=ExtendedEuclid(b, a mod b);
\end_layout

\begin_layout Plain Layout

	(d,x, y)=(d',y', x'-y'*floor(a/b));
\end_layout

\begin_layout Plain Layout

return (d,x, y);
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Per calcolare l'inverso moltiplicativo di 
\emph on
e 
\emph default
modulo 
\begin_inset Formula $\phi(n)$
\end_inset

, si può usare l'algoritmo ExtendedEuclid di cui sopra.
 Essendo 
\emph on
e
\emph default
 relativamente primo con 
\begin_inset Formula $\phi(n)$
\end_inset

, dovrà essere
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
1=ex+\phi(n)y
\]

\end_inset


\end_layout

\begin_layout Standard
dunque 
\begin_inset Formula $x$
\end_inset

 è l'inverso moltiplicativo di 
\emph on
e 
\emph default
modulo 
\begin_inset Formula $\phi(n)$
\end_inset

.
\end_layout

\begin_layout Subsubsection
\begin_inset CommandInset label
LatexCommand label
name "subsec:Esponenziazione-in-modulo"

\end_inset

Esponenziazione in aritmetica modulare
\end_layout

\begin_layout Standard
Sia la fase di cifratura anche quella di decifratura in RSA coinvolgono
 operazioni di elevamento a potenza di interi, con esponente intero, in
 modulo n.
 Se l'elevamento a potenza fosse effettuato sugli interi prima di ridurre
 in modulo 
\emph on
n
\emph default
, i risultati intermedi sarebbero giganteschi.
 Fortunatamente si può sfruttare le proprietà dell'aritmetica modulare,
 come
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
(a\cdot b)modn=(a\,mod\,n)\cdot(b\,mod\,n)
\]

\end_inset


\end_layout

\begin_layout Standard
Cioè, riducendo i risultati intermedi modulo
\emph on
 n
\emph default
 si può rendere più pratica l'elevazione a potenza.
\end_layout

\begin_layout Standard
Sfruttando la proprietà delle potenze, ossia
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
x^{a+b}=x^{a}\cdot x^{b}
\]

\end_inset


\end_layout

\begin_layout Standard
È possibile rendere ancora più pratica l'operazione di elevamento a potenza.
 Supponendo di voler calcolare 
\begin_inset Formula $a^{b}$
\end_inset

, possiamo esprimere l'esponente b come numero binario 
\begin_inset Formula $b={b_{k},b_{k-1},...,b_{0}}$
\end_inset

, per cui avremmo
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
b=\sum_{b_{i}\neq0}2^{i}
\]

\end_inset


\end_layout

\begin_layout Standard
quindi
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
a^{b}=a^{\sum_{b_{i}\neq0}2^{i}}=\prod_{b_{i}\neq0}a^{2^{i}}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
a^{b}mod\,n=\left(\prod_{b_{i}\neq0}a^{2^{i}}\right)mod\,n=\left(\prod_{b_{i}\neq0}\left(a^{2^{i}}\,mod\,n\right)\right)mod\:n
\]

\end_inset


\end_layout

\begin_layout Standard
possiamo, dunque, usare il seguente algoritmo, tratto da 
\begin_inset CommandInset citation
LatexCommand cite
key "key-4"

\end_inset

, per calcolare 
\begin_inset Formula $a^{b}$
\end_inset

, avendo espresso l'esponente 
\begin_inset Formula $b={b_{k},b_{k-1},...,b_{0}}$
\end_inset

 come numero binario.
\end_layout

\begin_layout Standard
\begin_inset listings
lstparams "numbers=left,numberstyle={\scriptsize},basicstyle={\scriptsize\ttfamily},showstringspaces=false,tabsize=4"
inline false
status open

\begin_layout Plain Layout

ModularExponentiation(a,b,n)
\end_layout

\begin_layout Plain Layout

c:=0;
\end_layout

\begin_layout Plain Layout

f:=1;
\end_layout

\begin_layout Plain Layout

for i:=k downto 0 do
\end_layout

\begin_layout Plain Layout

	c:=2*c;
\end_layout

\begin_layout Plain Layout

	f:=(f*f)mod n:
\end_layout

\begin_layout Plain Layout

	if b[i] = 1 then
\end_layout

\begin_layout Plain Layout

		c:=c+1;
\end_layout

\begin_layout Plain Layout

		f:=(f*a) mod n;
\end_layout

\begin_layout Plain Layout

return f;	
\end_layout

\end_inset


\end_layout

\begin_layout Subsubsection
Uso efficiente della chiave pubblica
\end_layout

\begin_layout Standard
Per velocizzare le operazioni di cifratura che utilizzino la chiave pubblica,
 può essere scelto 
\emph on
e
\emph default
 che minimizzi il numero di moltiplicazioni necessarie ad effettuare l'elevament
o a potenza.
 Tipicamente viene scelto un numero ottenuto sommando 1 ad una potenza del
 2, ad esempio 65537 che è uguale a 
\begin_inset Formula $2^{16}+1$
\end_inset

.
 Ovviamente, una volta scelto il numero 
\emph on
e
\emph default
, va verificato che sia relativamente primo con 
\begin_inset Formula $\phi(n)$
\end_inset

.
 Se ciò non fosse, la coppia di numeri primi utilizzati per calcolare 
\emph on

\begin_inset Formula $\phi(n)$
\end_inset


\emph default
 può essere scartata, selezionando due numeri primi diversi e rigenerando
 la coppia di chiavi.
\end_layout

\begin_layout Subsubsection
Uso efficiente della chiave privata
\end_layout

\begin_layout Standard
Per quanto riguarda, invece, il valore di, non è possibile fare un discorso
 simile a quello fatto nel paragrafo precedente.
 Un valore di d piccolo, o che sia appartenente ad un preciso sottoinsieme
 dei numeri interi, rende l'algoritmo vulnerabile agli attacchi a forza
 bruta.
 Esiste, comunque, un modo per velocizzare le operazioni di cifratura e
 decifratura che coinvolgono la chiave privata usando il teorema cinese
 del resto.
 
\end_layout

\begin_layout Standard
Volendo calcolare 
\begin_inset Formula $M=C^{d}mod\,n$
\end_inset

, tenendo presente che 
\begin_inset Formula $n=p\cdot q$
\end_inset

, possiamo calcolare i risultati intermedi 
\begin_inset Formula $V_{p}=C^{d}mod\,p$
\end_inset

 e 
\begin_inset Formula $V_{q}=C^{d}mod\,q$
\end_inset

.
 Definendo le quantità 
\begin_inset Formula $X_{p}=q\cdot(q^{-1}mod\,p)$
\end_inset

 e 
\begin_inset Formula $X_{q}=p\cdot(p^{-1}mod\,q)$
\end_inset

, con 
\begin_inset Formula $q^{-1}$
\end_inset

e 
\begin_inset Formula $p^{-1}$
\end_inset

, rispettivamente, inverso moltiplicativo di q modulo p ed inverso moltiplicativ
o di p modulo q, si può scrivere
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
M=(V_{p}\cdot X_{p}+V_{q}\cdot X_{q})mod\,n
\]

\end_inset


\end_layout

\begin_layout Standard
Usando il teorema di Fermat, il quale stabilisce che se 
\emph on
a
\emph default
 e 
\emph on
p
\emph default
 sono relativamente primi allora 
\begin_inset Formula $a^{p-1}=1\,mod\,p$
\end_inset

, è possibile dimostrare che
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
V_{p}=C^{d\,mod(p-1)}mod\,p\;\;\;V_{q}=C^{d\,mod(q-1)}mod\,q
\]

\end_inset


\end_layout

\begin_layout Standard
precalcolando le quantità 
\begin_inset Formula $d\,mod(p-1)$
\end_inset

, 
\begin_inset Formula $d\,mod(q-1)$
\end_inset

, 
\begin_inset Formula $X_{p}$
\end_inset

 ed 
\begin_inset Formula $X_{q}$
\end_inset

 è possibile completare l'elevamento a potenza quattro volte più velocemente
 rispetto al normale.
\end_layout

\begin_layout Subsection
Sicurezza di RSA
\end_layout

\begin_layout Standard
Gli approcci per attaccare l'algoritmo RSA sono quattro:
\end_layout

\begin_layout Itemize
forza bruta, che prevede di provare tutte le possibili chiavi private;
\end_layout

\begin_layout Itemize
attacchi matematici, i quali affrontano il problema di fattorizzare il prodotto
 di due numeri primi;
\end_layout

\begin_layout Itemize
timing attack, che fa uso del tempo di esecuzione dell'algoritmo di decriptazion
e;
\end_layout

\begin_layout Itemize
chosen ciphertext attack, il quale usa le proprietà dell'algoritmo RSA e
 le sfrutta a proprio vantaggio.
\end_layout

\begin_layout Standard
La difesa dagli attacchi a forza bruta è uguale, a prescindere dal particolare
 sistema di cifratura, e prevede di usare uno spazio delle chiavi il più
 ampio possibile, dunque maggiore è il numero di bit con il quale viene
 rappresentato 
\emph on
d
\emph default
 tanto più è difficile violare il cifrario.
 Comunque, siccome 
\emph on
d
\emph default
 è coinvolto nei calcoli per la generazione della chiave sia nel processo
 di cifratura e decifratura, maggiore è il numero di bit e più lento risulterà
 il processo.
\end_layout

\begin_layout Subsubsection
Attacchi matematici
\end_layout

\begin_layout Standard
È possibile identificare tre tipologie diverse di attacchi matematici ad
 RSA:
\end_layout

\begin_layout Enumerate
fattorizzare n in due numeri primi positivi, il che consente di calcolare
 la funzione 
\begin_inset Formula $\phi(n)$
\end_inset

 di Eulero e determinare l'inverso moltiplicativo 
\begin_inset Formula $d=e^{-1}mod\phi(n)$
\end_inset

;
\end_layout

\begin_layout Enumerate
determinare 
\begin_inset Formula $\phi(n)$
\end_inset

 direttamente, senza determinare i numeri primi 
\emph on
p
\emph default
 e 
\emph on
q;
\end_layout

\begin_layout Enumerate
determinare d direttamente, senza determinare prima 
\begin_inset Formula $\phi(n)$
\end_inset

.
\end_layout

\begin_layout Standard
Determinare 
\begin_inset Formula $\phi(n)$
\end_inset

 dato 
\emph on
n
\emph default
 equivale a dovere per fattorizzare 
\emph on
n
\emph default
.
 Con gli algoritmi attualmente conosciuti, questo problema è davvero molto
 costoso dal punto di vista temporale.
 Esso viene, per tale motivo, usato come benchmark per valutare la sicurezza
 di RSA.
 Tipicamente lo sforzo necessario viene misurato in MIPS-years, ossia anni
 di elaborazione usando un processore che esegue un milione di istruzioni
 al secondo, ossia circa 
\begin_inset Formula $3\cdot10^{13}$
\end_inset

 istruzioni annue.
 
\end_layout

\begin_layout Standard
Per ottenere una discreta sicurezza è necessario utilizzare chiavi binarie
 di almeno 2048 bit.
 Quelle a 512 bit sono ricavabili in poche ore.
 Le chiavi a 1024 bit, ancora oggi largamente utilizzate, non sono più consiglia
bili.
 La fattorizzazione di interi grandi, infatti, è progredita rapidamente
 mediante l'utilizzo di hardware dedicati, al punto che potrebbe essere
 possibile fattorizzare un intero di 1024 bit in un solo anno di tempo,
 al costo di un milione di dollari (un costo sostenibile per qualunque grande
 organizzazione, agenzia o intelligence).
\end_layout

\begin_layout Standard
Per evitare valori di 
\emph on
n
\emph default
 che possano essere fattorizzate più facilmente, gli inventori dell'algoritmo
 hanno suggerito alcuni vincoli da rispettare nella scelta dei numeri primi
 p e q:
\end_layout

\begin_layout Enumerate
p e q devono differire in lunghezza soltanto per pochi bit;
\end_layout

\begin_layout Enumerate
sia (p-1) che (q-1) devono essere espressi come prodotto di un numero primo
 molto grande;
\end_layout

\begin_layout Enumerate
gcd(p-1, q-1) deve essere piccolo
\end_layout

\begin_layout Standard
In aggiunta è stato dimostrato che se 
\begin_inset Formula $e<n$
\end_inset

 e 
\begin_inset Formula $d<n^{\frac{1}{4}}$
\end_inset

 allora 
\emph on
d
\emph default
 può essere facilmente determinato.
\end_layout

\begin_layout Subsubsection
Timing attack
\end_layout

\begin_layout Standard
È stato dimostrato che è possibile risalire alla chiave privata tenendo
 traccia di quanto un computer impieghi per la decifrazione di un messaggio.
 I timing-attack sono utilizzabili contro ogni sistema crittografico a chiave
 pubblica, non solo contro RSA e sono estremamente importanti perché arrivano
 da un punto dal quale non ci si aspetterebbe di poter subire un attacco.
 Inoltre è un attacco che fa solo uso di ciphertext.
\end_layout

\begin_layout Standard
L'attacco è molto semplice da capire.
 Si supponga che il sistema target dell'attacco esegua moltiplicazioni in
 modulo molto velocemente nella maggior parte dei casi ma, in alcuni casi,
 impiega molto più tempo.
 L'attacco procede bit per bit cominciando dal bit più significativo.
 
\end_layout

\begin_layout Standard
Si supponga che i primi j bit siano già conosciuti,le operazioni successive
 dipendono dai bit dell'esponente che ancora non sono noti.
 Se un bit è settato allora viene eseguita la moltiplicazione in modulo
 n.
 Per alcuni valori degli operandi, la moltiplicazione in modulo può essere
 estremamente lenta e l'attaccante sa quali essi siano quindi, osservando
 il tempo di esecuzione dell'algoritmo di decifratura, può dedurre quale
 sia il valore del bit.
 Nella pratica l'implementazione delle esponenziazione modulare non ha una
 variazione così estrema del tempo di esecuzione.
 In ogni caso, se la misura viene effettuata precisamente, il processo di
 esponenziazione può mostrare una leggera variabilità che rende praticabile
 questo tipo di attacchi.
\end_layout

\begin_layout Standard
Anche se attacchi di questo tipo sono davvero molto seri, è possibile prendere
 alcune semplici contromisure che li rendono impraticabili:
\end_layout

\begin_layout Itemize
constant exponentiation time: la funzione di elevamento a potenza impiega
 lo stesso tempo, qualsiasi input gli sia dato; questa modifica risolve
 il problema ma degrada le performance;
\end_layout

\begin_layout Itemize
random delay: performance migliori possono essere ottenute aggiungendo un
 ritardo casuale alla funzione di esponenziazione, per confondere l'attaccante;
 se non viene aggiunto ritardo sufficiente, l'attaccante può ancora ottenere
 la chiave privata collezionando più misure per compensare il ritardo introdotto
;
\end_layout

\begin_layout Itemize
blinding: il ciphertext viene moltiplicato per un numero casuale prima di
 effettuare l'esponenziazione.
 Questo processo fai in modo che l'attaccante non possa conoscere il ciphertext
 che sia processato e quindi non può procedere nell'analisi.
\end_layout

\begin_layout Standard
RSA Data Security Inc., uno dei principali implementatori di RSA, nonché
 ex detentore del brevetto su di esso, utilizza proprio il blinding per
 prevenire questa tipologia di attacchi.
 Tale tecnica si articola nei seguenti passi:
\end_layout

\begin_layout Enumerate
generare un numero segreto casuale r che sia compreso tra 0 ed n-1;
\end_layout

\begin_layout Enumerate
calcolare 
\begin_inset Formula $C'=C(r^{e})modn$
\end_inset

;
\end_layout

\begin_layout Enumerate
calcolare 
\begin_inset Formula $M'=C'^{e}modn$
\end_inset

;
\end_layout

\begin_layout Enumerate
calcolare 
\begin_inset Formula $M=M'r^{-1}modn$
\end_inset

, con 
\begin_inset Formula $r^{-1}$
\end_inset

 inverso moltiplicativo di 
\emph on
r
\emph default
 modulo 
\emph on
n
\emph default
.
\end_layout

\begin_layout Standard
È stato stimato che il blinding conduce ad una diminuzione delle performance
 che varia tra il due ed il dieci percento, rispetto all'algoritmo di decodifica
 normale.
\end_layout

\begin_layout Subsubsection
Chosen ciphertext attack e Optimal Asymmetric Encryption Padding (OAEP)
\end_layout

\begin_layout Standard
RSA è vulnerabile ad attacchi 
\emph on
choosen ciphertext
\emph default
, o CCA.
 Tale attacco prevede che l'attaccante possieda un certo numero di ciphertext
 ed i relativi plaintext decifrati con la chiave privata dell'obiettivo
 dell'attacco.
 L'attaccante, scelto un plaintext, lo cifra con la chiave pubblica del
 target, per cui, scegliendo accuratamente i blocchi da processare con la
 chiave privata del target, può ottenere informazioni utili alla crittoanalisi.Pe
r evitare questo tipo di attacchi, molte implementazioni utilizzano il padding
 del plaintext prima di cifrarlo.
 RSA Data Security usa uno schema detto
\emph on
 optimal asymmetric encryption padding
\emph default
, che, però, va molto oltre gli scopi di questi appunti.
\end_layout

\end_body
\end_document
